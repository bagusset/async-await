const express = require('express')
const app = express()
const port = 8001
const { Todo, User } = require('./models')

app.use(express.json())

app.get('/',(req,res) => {
    res.json({
        message: "Hello Selamat Datang semuanya di Training Sequelize"
    })
  })

app.get('/todos', (req, res) => { 
  Todo.findAll().then(todos => {
      res.status(200).json({ 
        todos 
      })
  })
})
//res.send = untuk mengirim txt ke API atau web server
//res.json = untuk mengirim json ke API atau web server
//res.status =  untuk mengirim status ke API atau web server
// perbedaan req.body dan req.params , req.query.

app.get('/todos/:id', (req, res) => {    
    const id = Todo.findByPk(req.params.id) //method sequelize fungsinya untuk menamilkan isi JSON dari sebuah id
    id.then(todo => {    
      if (!todo) {
          res.status(404).json({
            status: false,
            message: `Project with ID ${req.params.id} not found`
          })
      } else {
        res.status(200).json({    
        status: true,
        message: `Project with ID ${req.params.id} retrieved!`,
        data: todo
        })
      }
    }).catch(() => {
      res.status(400).json({
        message: 'Error message'
      })
    })
})

app.post('/todos', (req, res) => {
    Todo.create({
      name: req.body.name,
      description: req.body.description,
      due_at: req.body.due_at,
      user_id: req.body.user_id
    }).then(todo => {
      if(!todo.user_id ) {
        res.status(400).json({
          message: 'You should insert user_id'
        })
      } else {
          res.status(201).json({
          status: true,
          message: 'Project created!',
          data: todo
        })
      }
    })
  })

app.put('/todos/:id', (req, res) => {
    Todo.findByPk(req.params.id).then(todo => {
      todo.update({
        name: req.body.name,
        description: req.body.description,
        due_at: req.body.due_at,
        user_id: req.body.user_id
      }, {
        where: {
          id: req.params.id
        }
      }).then(() => {
        res.status(200).json({
          status: true,
          message: `Project with ID ${req.params.id} updated!`,
          data: todo 
        })
      })
    })
  })
  
app.delete('/todos/:id', (req, res) => {
    Todo.destroy({
      where: {
        id: req.params.id
      }
    }).then(() => {
      if(!req.params.id){
          res.status(400).json({
          message: `Project with ID ${req.params.id} is not found`
        })
      } else {
          res.status(301).json({
          message: `Project with ID ${req.params.id} is deleted`
        })
      }
    })
  })

app.get('/users', (req,res) => {
    User.findAll().then(user => {
        res.status(200).json({
            info: user
        })
    })
})

app.get('/users/:id', (req, res) => {
  const _id = User.findByPk(req.params.id)
  _id.then(user => {
    if(!user) {
        res.status(404).json({
        status: false,
        message: `Users with ID ${req.params.id} not found`
        })
    } else {
      res.status(200).json({
        status: true,
        message: `Users with ID ${req.params.id} retrieved!`,
        data: user
      })
    } 
  })
})

app.post('/users', (req, res) => {
  User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  }).then(user => {
    if(!user.email) {
        res.status(400).json({
        status:false,
        message: "you should insert email"
      })
    } else {
        res.status(201).json({
        status: true,
        message: 'Users created!',
        data: user 
      })
    }
  })
})

app.put('/users/:id', (req, res) => {
  User.findByPk(req.params.id).then(user => {
    user.update({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    }, {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        status: true,
        message: `Users with ID ${req.params.id} updated!`,
        data: user 
      })
    })
  })
})

app.delete('/Users/:id', (req, res) => {
  User.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    if(!req.params.id) {
        res.status(400).json({
        status:false,
        message: `Users with ID ${req.params.id} not found`
      })
    } else {
        res.status(301).json({
        message: `Project with ID ${req.params.id} is deleted`
      })
    }
  })
})
  
app.listen(port, () => console.log(`Listening on port ${port}`))