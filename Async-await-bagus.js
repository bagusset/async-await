const express = require('express')
const app = express()
const port = 8001;
const { Todo, User } = require('./models')

app.use(express.json())

app.get('/', (req, res) => {
    res.json ({
        message : "Hello Selamat Datang semuanya di Training Sequelize"
    })
})
app.get('/todos', async (req, res) => {
    try {
        const data = await Todo.findAll()
        res.status(200).json({
        status: true,
        message: 'Project retrieved!',
        data: data
        })
    } catch (e) {
        res.status(422).json({
        message: 'Error'
        })
    }
})

app.get('/todos/:id', async (req, res) => {    
    try {
        const todo = await Todo.findByPk(req.params.id)
        if(!todo) {
                res.status(404).json({
                status: false,
                message: `Project with ID ${req.params.id} not found`
              })
        } else {
                res.status(200).json({    
                status: true,
                message: `Project with ID ${req.params.id} retrieved!`,
                data: todo
            })
        }
    } catch (e) {
        res.status(422).json({
        message: 'Error'
        })
    }   
})

app.post('/todos', async (req, res) => {
    try {
        const data = await Todo.create({
            name: req.body.name,
            description: req.body.description,
            due_at: req.body.due_at,
            user_id: req.body.user_id 
        })
        if(!data.user_id) {
                res.status(400).json({
                status:false,
                message: 'You should insert user ID'
            })    
        } else {
                res.status(201).json({
                status: true,
                message: 'Projects created!',
                data: data
            })    
        } 
    } catch(e) {
        res.status(400).json({
            message: 'Error'
        })
    }
  })

app.put('/todos/:id', async (req, res) => {
    try {
        const _id = await Todo.findByPk(req.params.id)
        if(!_id) {
            res.status(404).json({
                status: false,
                message: `ID ${_id} not found`
            })
        } else {
                let dataUpdate = await Todo.update({
                    name: req.body.name,
                    description: req.body.description,
                    due_at: req.body.due_at,
                    user_id: req.body.user_id
                }, {
                    where: {
                    id: req.params.id
                    }
                })
                res.status(200).json({
                    status: true,
                    message: `Todos with ID ${req.params.id} is updated`,
                    data: dataUpdate
                })   
            }
        
    } catch (e) {
        res.status(400).json({
            status: false,
            message: 'Error'
        })
    }
})
  
app.delete('/todos/:id', async (req, res) => {
    try {
        await Todo.destroy({
            where: {
              id: req.params.id
            }
        })
        if(!req.params.id){
            res.status(400).json({
            message: `Project with ID ${req.params.id} is not found`
          })
        } else {
            res.status(301).json({
            message: `Project with ID ${req.params.id} is deleted`
          })
        }
    } catch(e) {
        res.status(400).json({
            message: 'Error message'
        })
    } 
})

app.get('/users', async (req, res) => {
    try {
        const data = await User.findAll()
        res.status(200).json({
        status: true,
        message: 'Project retrieved!',
        data: data
        })
    } catch (e) {
        res.status(422).json({
        message: 'Error'
        })
    }
})

app.get('/users/:id', async (req, res) => {    
    try {
        const user = await User.findByPk(req.params.id)
        if(!user) {
                res.status(404).json({
                status: false,
                message: `Project with ID ${req.params.id} not found`
              })
        } else {
                res.status(200).json({    
                status: true,
                message: `Project with ID ${req.params.id} retrieved!`,
                data: user
            })
        }
    } catch (e) {
        res.status(422).json({
        message: 'Error'
        })
    }   
})

app.post('/users', async (req, res) => {
    try {
        const data = await User.create({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        })

        if(!data.email) {
                res.status(400).json({
                status:false,
                message: 'You should insert user ID'
            })    
        } else {
                res.status(201).json({
                status: true,
                message: 'Projects created!',
                data: data
            })    
        } 
    } catch(e) {
        res.status(400).json({
            message: 'Error'
        })
    }
  })

app.put('/users/:id', async (req, res) => {
    try {
        const _id = await User.findByPk(req.params.id)
        if(!_id) {
            res.status(404).json({
                status: false,
                message: `Please insert ID`
            })
        } else {
                let dataUpdate = await User.update({
                    name: req.body.name,
                    email: req.body.email,
                    password: req.body.password
                }, {
                    where: {
                    id: req.params.id
                    }
                })
                res.status(200).json({
                    status: true,
                    message: `Todos with ID ${req.params.id} is updated`,
                    data: dataUpdate
                })   
            }
        
    } catch (e) {
        res.status(400).json({
            status: false,
            message: 'Error'
        })
    }
})
  
app.delete('/users/:id', async (req, res) => {
    try {
        await User.destroy({
            where: {
              id: req.params.id
            }
        })
        if(!req.params.id){
            res.status(400).json({
            message: `Project with ID ${req.params.id} is not found`
          })
        } else {
            res.status(301).json({
            message: `Project with ID ${req.params.id} is deleted`
          })
        }
    } catch(e) {
        res.status(400).json({
            message: 'Error message'
        })
    } 
})
  
app.listen(port, () => console.log(`Listening on port ${port}`))